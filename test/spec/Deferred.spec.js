var Deferred = require('../../index.js');

var def = new Deferred();

describe('`Deferred` interface', function () {

  it('should export `resolve` method', function() {
      expect(typeof def.resolve).toBe('function');
  });

  it('should export `reject` method', function() {
      expect(typeof def.reject).toBe('function');
  });

  it('should export `done` method', function() {
      expect(typeof def.done).toBe('function');
  });

  it('should export `fail` method', function() {
      expect(typeof def.fail).toBe('function');
  });

  it('should export `always` method', function() {
      expect(typeof def.always).toBe('function');
  });

  it('should export `then` method', function() {
      expect(typeof def.then).toBe('function');
  });

  it('should export `Deferred.isPromise` method', function() {
      expect(typeof Deferred.isPromise).toBe('function');
  });

  it('should export `Deferred.when` method', function() {
      expect(typeof Deferred.when).toBe('function');
  });

});

describe('`resolve` method', function() {
    var def = new Deferred();
    it('the state of Deferred should be "resolved"', function() {
        def.resolve();
        expect(def.state).toEqual("resolved");
    });
});

describe('`reject` method', function() {
    var def = new Deferred();
    it('the state of Deferred should be "rejected"', function() {
        def.reject();
        expect(def.state).toEqual("rejected");
    });
});

describe('`done` method', function() {

  describe('single done-listener', function () {

    var def = new Deferred();
    var done = jasmine.createSpy();
    var sync = false;

    def.done(done);

    it('the callback function "done" should be executed', function() {

      runs(function () {
        setTimeout(function () {
          def.resolve();
          sync = true;
        }, 100);
      });

      waitsFor(function () {
        return sync;
      }, 'the function `done` should be executed', 200);

      runs(function () {
        expect(done).toHaveBeenCalled();
      });

    });

  });

  describe('multi done-listener', function () {

    var defer = new Deferred();

    var value = 10;
    var count = 0;

    var done = function (value) {
      count += value;
    };

    var fail = function (value) {
      count -= value;
    };

    var sync = false;

    for (var i = 0; i < 10; i++) {
      defer.done(done);
    }

    for (var i = 0; i < 10; i++) {
      defer.fail(fail);
    }

    it(
      'all 10 done listeners should be called when defer resolved',
      function () {

        runs(function () {
          setTimeout(function () {
            defer.resolve(value);
            sync = true;
          }, 100);
        });

        waitsFor(function () {
          return sync;
        }, 'the function `done` should be executed', 200);

        runs(function () {
          expect(count).toBe(100);
        });

      }
    );

  });

});

describe('`fail` method', function() {
  var def = new Deferred();
  var failure = jasmine.createSpy();
  def.fail(failure);
  def.reject();
  it('the callback function "failure" should be executed', function() {
      expect(failure).toHaveBeenCalled();
  });
});

describe('`always` method', function() {

  var def = new Deferred();
  var always = jasmine.createSpy();

  def.always(always);

  Math.round(Math.random()) === 1 ? def.resolve() : def.reject();
  it(
    'resolved or rejected, the callback function "always" should be executed', 
    function() {
      expect(always).toHaveBeenCalled();
    }
  );

});

describe('`then` method', function() {

  describe('resolved', function () {

    var done = jasmine.createSpy();
    var fail = jasmine.createSpy();
    var def = new Deferred();

    def.then(done, fail);
    def.resolve();

    it('resolved, execute `done`', function() {
      expect(done).toHaveBeenCalled();
    });

  });

  describe('rejected', function () {

    var done = jasmine.createSpy();
    var fail = jasmine.createSpy();
    var def = new Deferred();

    def.then(done, fail);
    def.reject();

    it('rejected, execute `fail`', function () {
      expect(fail).toHaveBeenCalled();
    });

  });

});

describe('`then` chaining', function () {

  var defer1 = new Deferred();
  var defer2 = new Deferred();
  var sync = 0;
  var result = 0;

  defer1.done(function (a) {
    return defer2.done(function (b) {
      result = a + b;
    });
  });

  it('chaing shoud be call one by one', function () {

    runs(function () {

      setTimeout(function () {
        defer1.resolve(1);
        sync++;
      }, 100);

      setTimeout(function () {
        defer2.resolve(2);
        sync++;
      }, 200);

    });

    waitsFor(function () {
      return sync === 2;
    }, 'the function `done` should be executed but timeout', 300);

    runs(function () {
      expect(result).toBe(3);
    });

  });


});

describe('`Deferred.isPromise` method', function() {
    var def = new Deferred();
    Deferred.isPromise(def);
    it('def should be a promise object', function() {
        expect(Deferred.isPromise(def)).toBeTruthy();
        expect(Deferred.isPromise({})).toBeFalsy();
    });
});

describe('`when` method', function() {

  it('should be defined on `Deferred` object', function () {
      expect(typeof Deferred.when).toBe('function');
  });

  it('should return a promise object', function() {
      expect(Deferred.isPromise(Deferred.when())).toBeTruthy();
  });

  describe('should be resolved when all promises resolved', function () {

    var done = jasmine.createSpy('when-resolved');
    var fail = jasmine.createSpy('when-rejected');
    var always = jasmine.createSpy('when-always');

    var defer1 = new Deferred();
    var defer2 = new Deferred();

    Deferred
      .when(defer1.promise, defer2.promise)
      .done(done)
      .fail(fail)
      .always(always);

    defer1.resolve();
    defer2.resolve();

    it('should be resolved when all promises resolved', function () {
      expect(done).toHaveBeenCalled();
    });

    it(
      'reject listener should not called when all promises rejected', 
      function () {
        expect(fail).not.toHaveBeenCalled();
      }
    );

    it(
      'always listener should be called',
      function () {
        expect(always).toHaveBeenCalled();
      }
    );

  });

  describe('should be rejected when any promises rejected', function () {

    var done = jasmine.createSpy('when-resolved');
    var fail = jasmine.createSpy('when-rejected');
    var always = jasmine.createSpy('when-always');

    var defer1 = new Deferred();
    var defer2 = new Deferred();

    var promise = Deferred
      .when(defer1.promise, defer2.promise)
      .done(done)
      .fail(fail)
      .always(always);


    defer1.resolve();
    defer2.reject();

    it(
      'resolve listener should not called when all promises rejected', 
      function () {
        expect(done).not.toHaveBeenCalled();
      }
    );

    it('should be resolved when all promises resolved', function () {
        expect(fail).toHaveBeenCalled();
    });

    it(
      'always listener should be called',
      function () {
        expect(always).toHaveBeenCalled();
      }
    );

  });

});