module.exports = Deferred;

function Deferred() {

  this.state = 'unfulfilled';
  this.listeners = [];

  this.promise = {
      done: this.done.bind(this),
      fail: this.fail.bind(this),
      always: this.always.bind(this),
      then: this.then.bind(this)
  };

}

function notifyAll(isError, value, listeners) {
  for(var i = 0, len = listeners.length; i < len; i++){
    notify(isError, value, listeners[i]);
  }
}

function notify(isError, value, listener) {

  var callback = isError ? listener.fail : listener.done;
  var defer = listener.defer;

  if (callback) {
    process.nextTick(function () {

      try {
        var result = callback(value);

        if (Deferred.isPromise(result)) {
          listener.defer.then(defer.resolve, defer.reject);
          return;
        }

        defer.resolve(result);

      } catch (e) {
        defer.reject(e);
      }

    });
  }
  else {
    isError ? defer.reject(value) : defer.resolve(value);
  }

}

Deferred.prototype.resolve = function (value) {

  if (this.state !== 'unfulfilled') {
      throw new Error('this Deferred is already ' + this.state);
  }

  this.value = value;
  this.state = 'resolved';

  notifyAll(false, value, this.listeners.slice());

  // 解除对回调函数的引用
  this.listeners = null;

};

Deferred.prototype.reject = function (value) {

  if (this.state !== 'unfulfilled') {
    throw new Error('this Deferred is already ' + this.state);
  }

  this.value = value;
  this.state = 'rejected';

  notifyAll(true, value, this.listeners.slice());

  // 解除对回调函数的引用
  this.listeners = null;

};

/**
 * 添加一个成功回调函数
 * 
 * @param {Function} done 成功回调函数
 * @return {Promise} this.promise
 */
Deferred.prototype.done = function (done) {
    return this.then(done, null);
};

/**
 * 添加一个失败回调函数
 * 
 * @param {Function} fail 失败回调函数
 * @return {Promise} this.promise
 */
Deferred.prototype.fail = function (fail) {
    return this.then(null, fail);
};

/**
 * 添加一个回调函数
 * 一个不论最后的状态是resolved或是rejected都进行回调
 * 
 * @param {Function} always 回调函数
 * @return {Promise} this.promise
 */
Deferred.prototype.always = function (always) {
    return this.then(always, always);
};

/**
 * 添加成功和失败回调函数
 * 
 * @param {Function} done 成功回调函数
 * @param {Function} fail 失败回调函数
 * @return {Promise} this.promise
 */
Deferred.prototype.then = function (done, fail) {

  // 为每个then绑定生成一个新的defer对象
  // 以支持回调中返回新的promise时，可以连接在一起
  var defer = new Deferred();

  var listener = {
    done: done,
    fail: fail,
    defer: defer
  };

  if (this.state === 'unfulfilled') {
    this.listeners.push(listener);
    return defer.promise;
  }

  if (this.state === 'resolved') {
    notify(false, this.value, listener);
    return this.promise;
  }

  if (this.state === 'rejected') {
    notify(true, this.value, listener);
    return this.promise;
  }

};

/**
 * 输入多个Promise对象
 * 当所有的Promise对象都被resolve或reject时，转换成resolve/reject状态
 * 
 * @return {Promise} promise
 */
Deferred.when = function () {

  var defer = new Deferred();
  var promises = [].slice.call(arguments);
  var count = promises.length;

  var done = function () {
    if (--count === 0) {
      done = fail = null;
      promises = null;
      defer.resolve();
    }
  };

  var fail = function () {
    done = fail = null;
    promises = null;
    defer.reject();
  };

  for (var i = 0; i < count; i++) {
    promises[i].then(done, fail);
  }

  return defer.promise;

};

/**
 * 判断一个对象是不是符合Promise接口的
 * 
 * @param {Object} promise 判断对象
 * @return {boolean}
 */
Deferred.isPromise = function (promise) {
    return promise && typeof promise.then === 'function';
};